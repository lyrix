require 'fileutils'
require 'csv'
namespace 'joyent_slingshot' do  
  task :sync_up do    
     puts "Syncing upward" 
     puts "#{RAILS_ROOT}" 
     require "#{RAILS_ROOT}/config/environment.rb"     
     
     ActiveRecord::Base.default_timezone = :utc 
     ActiveRecord::Base.record_timestamps = false        
     
     sURI = URI.parse(ENV["SYNC_CONTROLLER"])
     puts "offline mode is #{ENV['OFFLINE_MODE']}"
     if !ENV['OFFLINE_MODE'].nil?
        puts "online mode"
     end     
   
  
     lastSyncLog = SyncLog.find(:first, :order => "id DESC", :conditions => "sync_direction = 'down'")
          
     if lastSyncLog.nil?
       raise "No synclog found, this shouldn't happen -- up sync can only happen after a down"         
     end
     
     lastCSVBlock = lastSyncLog.csv_block.strip
     
     filterDate = lastSyncLog.sync_time

     
     
     #filterDate = Time.parse("2007-03-19T23:35:00Z<")
   
     model = [Song.find(:all)]
     model += Show.find(:all)
     model += Usage.find(:all)

    
     xml = "<SlingshotUp>\n"
     xml << "<CreatedRecords>\n"     
     append_created(model, filterDate, nil, xml)
     xml << "</CreatedRecords>\n"
         
     xml << "<UpdatedRecords>\n"     
     append_updated(model, filterDate, nil, xml)
     xml << "</UpdatedRecords>\n"     
     
     xml << "<DeletedRecords>"     
     append_deleted(model, lastCSVBlock, xml)
     xml << "</DeletedRecords>\n"
      
     xml << "</SlingshotUp>\n"
   
     puts xml
     http = Net::HTTP.new(sURI.host, sURI.port)
     http.start do |http|
       request=Net::HTTP::Post.new(sURI.path + "/up") 
       request.set_form_data({'xml' => xml}, ';')
       request.basic_auth sURI.user, sURI.password
       response = http.request(request)    
       puts "response.body!! #{response.body} Response over!!"
     end
      
     Rake::Task['joyent_slingshot:sync_down'].invoke
     

         
  end 
  
  task :sync_down_files do
    puts "Syncing files downward"
    require "#{RAILS_ROOT}/config/environment.rb"

    ActiveRecord::Base.default_timezone = :utc
    ActiveRecord::Base.record_timestamps = false

    sURI = URI.parse(ENV["SYNC_CONTROLLER"])

    xml =[]
    http = Net::HTTP.new(sURI.host, sURI.port)
    http.start do |http|
      request=Net::HTTP::Post.new(sURI.path + "/files")
      request.set_form_data({'filterDate' => '1970-01-01'}, ';')
      request.basic_auth sURI.user, sURI.password
      response = http.request(request)
      xml = REXML::Document.new(response.body)
    end



    xml.elements.each("SlingshotFilesDown/FileLinksBlock/*") do |elem|
      puts "Downloading #{elem.text}"
    end


  end


    
  task :sync_down do
    puts "Syncing downward"
    puts "#{RAILS_ROOT}"


    require "#{RAILS_ROOT}/config/environment.rb"

    ActiveRecord::Base.default_timezone = :utc
    ActiveRecord::Base.record_timestamps = false

    puts "offline mode is #{ENV['OFFLINE_MODE']}"
    if !ENV['OFFLINE_MODE'].nil?
      puts "online mode"
    end

    sURI = URI.parse(ENV["SYNC_CONTROLLER"])

    xml =[]
    http = Net::HTTP.new(sURI.host, sURI.port)
    puts sURI
    http.start do |http|
      request=Net::HTTP::Post.new(sURI.path + "/down")
      request.set_form_data({'filterDate' => '1970-01-01'}, ';')
      request.basic_auth sURI.user, sURI.password
      response = http.request(request)
      puts "response.body #{response.body}"
      xml = REXML::Document.new(response.body)
    end
    File.open("log/sync_down_xml.xml", "w") {|f| f.write xml }


    ## Block to handle deletions
    csvBlock = []
    xml.elements.each("SlingshotDown/ValidIDBlock") do |elem|
          puts "Doing deletions if necessary"
          csvBlock = elem
          CSV.parse(elem.text.strip).each do |csvRow|
              className = csvRow.shift.singularize.titlecase.gsub(' ', '')
              csvRow.sort! {|x,y| x.to_i <=> y.to_i}
      
              klassName = Object.const_get(className)
      
              puts "klassName #{klassName}"
      
              localRecords = klassName.find(:all, :order => "id ASC")
              idsToDelete = []
              counter = 0
      
              while(counter < localRecords.size) do
                localRecord = localRecords[counter]
                remoteid = csvRow.shift.to_s
                if localRecord.id.to_s < remoteid || remoteid == ""
                  idsToDelete << localRecord.id
                  puts "deleting record from #{className} with id #{remoteid} no match #{localRecord.id}"
                  csvRow.unshift(remoteid)
                  counter = counter + 1
                end
      
                if localRecord.id.to_s  == remoteid
                  counter = counter + 1
                end
              end
              klassName.delete(idsToDelete)
            end
        end

    # block to handle update/creates

    xml.elements.each("SlingshotDown/DataBlock/*") do |elem|
      elemName = elem.name.singularize.titlecase.gsub(' ', '')

      puts "Beginning transaction on #{elemName}"

      Object.const_get(elemName).transaction do
        dRecords = Object.const_get(elemName).from_xml(elem)
        dRecords.each do |dRecord|
          dRecord.save
        end
      end
      puts "Finished transaction on #{elemName}\n"
    end

  
    # Post that we successfully sync'd down that data packet marked as SyncTime
    xml.elements.each("SlingshotDown/SyncTime") do |elem|
      req = Net::HTTP::Post.new(sURI.path + "/log")
      req.basic_auth sURI.user, sURI.password
      req.set_form_data({'sync_time' => elem.text})
      res = Net::HTTP.new(sURI.host, sURI.port).start {|http| http.request(req) }
      puts res.body
      r = SyncLog.create(:sync_time => elem.text, :sync_direction => 'down', :csv_block => csvBlock.text, :status => true)  
      puts "Saving local synclog #{r.to_xml}"
    end
  end

  ######
  private
  ######
  def append_created(array, filterDate, except, xml)
    workingName = ""
    array.each do |model|
      if model.instance_of?(Array)
        model.each do |row|
          if !row.nil? && ((row.respond_to?("created_at") && !row.created_at.nil? && row.created_at > filterDate) || !row.respond_to?("created_at") || row.created_at.nil?)
            if workingName != model[0].class.name.underscore.pluralize.dasherize && workingName != ""
              xml << "</#{workingName}>\n"
              workingName = model[0].class.name.underscore.pluralize.dasherize
              xml << "<#{workingName}>\n"
            end

            if workingName == ""
              workingName = model[0].class.name.underscore.pluralize.dasherize
              xml << "<#{workingName}>\n"
            end
            xml << row.to_xml(:skip_instruct => true, :except => except)
            #  row.destroy
          end
        end
      else
        if !model.nil? && ((model.respond_to?("created_at") && !model.created_at.nil? && model.created_at > filterDate) || !model.respond_to?("created_at") || model.created_at.nil?)
          if workingName != model.class.name.underscore.pluralize.dasherize
            xml << "</#{workingName}>\n" unless workingName == ""
            workingName = model.class.name.underscore.pluralize.dasherize
            xml << "<#{workingName}>\n"
          end
          xml << model.to_xml(:skip_instruct => true, :except => except)
          # model.destroy
        end
      end
    end
    if !workingName.nil? && workingName != ""
      xml << "</#{workingName}>\n"
    end
  end


  def append_updated(array, filterDate, except, xml)
    workingName = ""
    array.each do |model|
      if model.instance_of?(Array)
        model.each do |row|
          if !row.nil? && ((row.respond_to?("created_at") && !row.created_at.nil? && row.created_at < filterDate && !row.updated_at.nil? && row.updated_at > filterDate) || !row.respond_to?("created_at") || row.created_at.nil?)
            if workingName != model[0].class.name.underscore.pluralize.dasherize && workingName != ""
              xml << "</#{workingName}>\n"
              workingName = model[0].class.name.underscore.pluralize.dasherize
              xml << "<#{workingName}>\n"
            end

            if workingName == ""
              workingName = model[0].class.name.underscore.pluralize.dasherize
              xml << "<#{workingName}>\n"
            end
            xml << row.to_xml(:skip_instruct => true, :except => except)
          end
        end
      else
        if !model.nil? && ((model.respond_to?("created_at") && !model.created_at.nil? && model.created_at < filterDate && !model.updated_at.nil? && model.updated_at > filterDate) || !model.respond_to?("created_at") || model.created_at.nil?)
          if workingName != model.class.name.underscore.pluralize.dasherize
            xml << "</#{workingName}>\n"  unless workingName == ""
            workingName = model.class.name.underscore.pluralize.dasherize
            xml << "<#{workingName}>\n"
          end
          xml << model.to_xml(:skip_instruct => true, :except => except)
        end
      end
    end
    if !workingName.nil? && workingName != ""
      xml << "</#{workingName}>\n"
    end
  end

  def append_deleted(model, remoteCSVBlock, xml)
    localCSVBlock = []
    # get a csv block for the current model
    append_csv(model, localCSVBlock)
    remote = CSV.parse(remoteCSVBlock)
    local = CSV.parse(localCSVBlock.to_s)

    localHash = Hash.new()
    local.each do |localRow|
      localClassName = localRow.shift.singularize.titlecase.gsub(' ', '')
      localHash[localClassName.to_s] = localRow.sort {|x,y| x.to_i <=> y.to_i}
    end
   
   
    
    remote.each do |remoteRow|
      remoteClassNameRaw = remoteRow.shift
      remoteClassName = remoteClassNameRaw.singularize.titlecase.gsub(' ', '')
      localRow = localHash[remoteClassName.to_s]
      if !localRow.nil?
        localRow.sort! {|x,y| x.to_i <=> y.to_i}
        remoteRow.sort! {|x,y| x.to_i <=> y.to_i}
        idsToDelete = ""
        counter = 0

        while(counter < remoteRow.size) do
          remoteid = remoteRow[counter]
          localid = localRow.shift.to_s
          if remoteid < localid || localid == ""
            idsToDelete << "," << remoteid
            localRow.unshift(localid)
            counter = counter + 1
          end
          if remoteid  == localid
            counter = counter + 1
          end
        end

        if idsToDelete != "" 
          xml << remoteClassNameRaw.to_s << idsToDelete <<  "\n"
        end

      end
    end
  end

  def append_csv(array, csv)
    workingName = ""
    array.each do |model|
      if model.instance_of?(Array)
        model.each do |row|
          if workingName != model[0].class.name.underscore.pluralize.dasherize && workingName != ""
            workingName = model[0].class.name.underscore.pluralize.dasherize
            csv << "\n#{workingName}"
          end

          if workingName == ""
            workingName = model[0].class.name.underscore.pluralize.dasherize
            csv << "#{workingName}"
          end

          csv << "," << row.id
        end
      else
        if !model.nil?

          if workingName != model.class.name.underscore.pluralize.dasherize
            workingName = model.class.name.underscore.pluralize.dasherize
            csv << "\n#{model.class.name.underscore.pluralize.dasherize}"
          end
          csv << ","  << model.id
        end
      end
    end
    csv << "\n"
  end



   
  
  
end

