#
# Copyright (c) 2007 Joyent Inc.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

require 'slingshot'
require 'app/models/sync_log'
require 'app/controllers/slingshot_controller'
ActiveRecord::Base.send :include, Slingshot::XMLDeSerialize



unless SyncLog.table_exists?
  ActiveRecord::Schema.create_table(SyncLog.table_name) do |t|
    t.column :sync_time,       :datetime
    t.column :sync_direction,  :string
    t.column :csv_block,       :text
    t.column :status,          :boolean, :default => false, :null => false
  end
end