#
# Copyright (c) 2007 Joyent Inc.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

require 'app/models/sync_log'
require 'app/controllers/slingshot_controller'

module Slingshot
  
  module XMLDeSerialize
    
    def self.included(base)
      base.extend(ClassMethods)
    end
  
    # XML Deserializer for ActiveRecord
    # by Wayne Robinson and Dominic Orchard
    module ClassMethods
      def from_xml(xml)
        if xml.class == String
            # If passed a string, convert to XML object, and set root
            xml = REXML::Document.new(xml) 
            root = xml.elements[1]
        else
            # If already passed an XML object, then set root to XML object
            root = xml
        end
      
        if  ((root.name.underscore != self.class_name.underscore) and 
               (root.name.underscore != self.class_name.pluralize.underscore))
                # Check the top level is actual refering to the class
                # e.g. , for class Customer
               return 'nil'
        end
      
        # Deal with XML data containing many record instances
          if (root.name.underscore == self.class_name.pluralize.underscore and self.class_name.pluralize.underscore!=self.class_name.underscore) or root.name==root.elements[1].name
          
              root.elements.inject([]) do |instances, element|

                  elem = self.from_xml(element)
               
               
                  instances.push(elem)
              end

          else
              # Try to retrieve from ID in
              # XML data and update this record or start a new record
              # Find an id element in the elements
              id_element = root.elements.inject(nil) do |found, element|
                    if element.name=="id"
                          element
                    else
                          found
                    end
              end
              # if we haven't found the ID element
              if id_element.nil?
                    new_record = self.new
              else
                    # Retrieve from XML
                    begin            
                        new_record = self.find(id_element.text.to_i)                  
                    rescue
                        # If that record in fact didn't exist... start a new one
                        new_record = self.new
                    end
              end
            
              # Iterate through elements
              root.elements.each do | element |
                  sym = element.name.underscore.to_sym
      
                  # An association
                if element.has_elements?
  
                  setter = (sym.to_s+"=")
                  # Check the setter is an instance method
                  if self.instance_methods.member?(setter)
                        klass = self.reflect_on_association(sym).klass
                       from = klass.from_xml(element)
                       new_record.__send__(setter.to_sym, from)
                       puts new_record.addresses[0].country_name
                  end
    
                # An attribute
                else
                    # Check that the attribute is actual part of the record
                    if new_record.attributes.member?(sym.to_s) || sym==:id
                        if element.text.nil?              
                              col = new_record.column_for_attribute(sym)
                              # Handle an empty element with a not null column
                              if !col.null
                                  # Use default value 
                                  new_record[sym] = col.default
                                
                              end
                        else                      
                              new_record[sym] = element.text
                        end
                    end
               end
            end
        
            new_record
          end
       end
       
     end
     
  end
  
  
end