#
# Copyright (c) 2007 Joyent Inc.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


class SlingshotController < ActionController::Base
  layout nil

  #
  #  This method takes takes the parameter "xml" and parses out the active record objects that were sent
  #  Created records need to be inserted and assigned new id values so that they are globally unique
  #  make note of the use sanitize_records function, that you'll want to modify so that you can protected against
  #  unruly modifications or a rouge client. In particular, you'll likely want to sanitize relationships between objects
  #  so that a foreign key is pointing to an object that a given user actually has permissions for within your app
  #
  #  Need to modify: None
  #  Support functions: santize_records
  def up
    xmlPacket = params['xml']
    @xml = ""
    recordGroups = REXML::Document.new(xmlPacket)
    #this is a tad ugly
    recordGroups.elements.each("SlingshotUp/CreatedRecords/*/*") do |elem|
      singleObjectString = elem.name.titlecase.gsub(' ', '')
      singleObject = Object.const_get(singleObjectString)
      newID = 0
      # This is the only real way [with MYSQL] to increment the primary key counter so I can then modify the XML while i reset foreign keys
      # Unfortuantely ghetto
      begin
        singleObject.transaction do
          newObject = singleObject.from_xml(elem)
          newObject.id = nil
          newObject.save
          newID = newObject.id
          raise
          # ActiveRecord::Base.connection.next_sequence_value(singleObject.sequence_name)
        end
      rescue
      end

      oldID = elem.elements["id"].text
      elem.elements["id"].text = newID;

      #not efficient, but XPath is being a pain
      REXML::XPath.each( recordGroups, "SlingshotUp//#{elem.name}-id") do |element|
        if element.text == oldID
          element.text = newID
        end
      end
    end
    @xml = recordGroups.to_s

    recordGroups.elements.each("SlingshotUp/CreatedRecords/*") do |elem|
      elemName = elem.name.singularize.titlecase.gsub(' ', '')
      recordGroup = Object.const_get(elemName).from_xml(elem)
      if sanitize_records(recordGroup)
        recordGroup.each do |singleRecord|
          singleRecord.save
        end
      end
    end

    recordGroups.elements.each("SlingshotUp/UpdatedRecords/*") do |elem|
      elemName = elem.name.singularize.titlecase.gsub(' ', '')
      recordGroup = Object.const_get(elemName).from_xml(elem)
      if sanitize_records(recordGroup)
        recordGroup.each do |singleRecord|
          singleRecord.save
        end
      end
    end
    
    require 'csv'
    
    recordGroups.elements.each("SlingshotUp/DeletedRecords/") do |elem|
      if elem.text.nil?
        return
      end    
      toDelete = CSV.parse(elem.text)  
      if !sanitize_deletes(toDelete)
        return
      end
      
      toDelete.each do |csvRow|
        className = csvRow.shift.singularize.titlecase.gsub(' ', '')       
        klassName = Object.const_get(className)    
        klassName.delete(csvRow)
      end        
    end
    
    respond_to do |format|
      format.html { render :xml => @xml }
      format.xml  { render :xml => @xml }
    end
    
  end


  # This function is responsible for generating the xml packet that contains all the relevant activerecord objects
  # Make sure to do most of your modifications in the aggregate_data section
  # you're probably not going to want to modify too much in here, look at the aggregate_data function where you should
  # assemble the objects that are going to be based down to the user.
  def down
    full_sync = params['fullSync']
    if (params['filterDate'].nil?)
      filterDate = SyncLog.find(:first, :order => "id DESC")
      if !filterDate.nil?
        filterDate = filterDate.sync_time
      else
        filterDate = Time.parse("1970-01-01")
        full_sync = 'yes'
      end
    else
      filterDate = Time.parse(params['filterDate'])
    end
    filterDate = Time.parse("1970-01-01")
    full_sync = 'yes'
    @csv = []
    @xml = []


    data_packet = aggregate_data
    
    if full_sync == 'yes'
      append_xml(data_packet, filterDate, :full_sync)
      append_csv(data_packet)
    else
      append_xml(data_packet, filterDate, :differential_sync)
      append_csv(data_packet)
    end

    @sync_time = Time.now.utc
    
    respond_to do |format|
      format.html { render :xml => wrap_packet }
      format.xml  { render :xml => wrap_packet }
    end
    
  end

  #
  #  Helper method specific to connector so that we can download a file using only basic auth.
  #  Current download methods in connector's file controller require different authentication
  #
  def download_file
    # Example
    # @joyent_file = JoyentFile.restricted_find(params[:id])
    #     send_joyent_file(@joyent_file, true)
    #   rescue ActiveRecord::RecordNotFound
    #     redirect_to files_index_url
  end


  # You're going to want to provide download links within aggregate_file_links that can be downloaded using basic/digest auth
  # Since it is likely a client will want to download files while they are not authenticated via a web session [eg, overnight]
  def files
    @syncTime = Time.now.utc
    @files = []
    aggregate_file_links
  end


  #
  #  Call this function to notify the server of a successful sync downwards so that we can record it in our sync log
  #  This becomes important for differential diffing
  #
  def log
    raise "Provide a syncTime" if params['sync_time'].nil?
    r = SyncLog.create(:sync_time => params['sync_time'], :sync_direction => 'down')
    r.save
    render :xml => r.to_xml
  end

  private
    
    def wrap_packet
      x = Builder::XmlMarkup.new(:indent => 2)
      x.instruct!
      x.SlingshotDown do |d|
        d.CurrentMigration ActiveRecord::Migrator.current_version
        d.SyncTime  @sync_time
        d.ValidIDBlock @csv
        d.DataBlock do |db|
          db << @xml.to_s
        end
      end
    end

    def aggregate_file_links
      files = []
      files.each do |file|
        @files << "\n<FileLink>"
        link = url_for :controller => "sync", :action => "download_file", :id => file.id
        @files << link
        @files << "</FileLink>\n"
      end
    end


    #
    # This function generates a list of id's and formats them as a CSV. This is so that the offline client can know
    # what to delete from the database. It will always represent all the ids that are valid, wherease the following xml data block
    # only represents updates/creates since last sync [assuming differential sync is being used]
    def append_csv(array)
      workingName = ""
      array.each do |model|
        if model.instance_of?(Array)
          model.each do |row|
            if workingName != model[0].class.name.underscore.pluralize.dasherize && workingName != ""
              workingName = model[0].class.name.underscore.pluralize.dasherize
              @csv << "\n#{workingName}"
            end

            if workingName == ""
              workingName = model[0].class.name.underscore.pluralize.dasherize
              @csv << "#{workingName}"
            end

            @csv << "," << row.id
          end
        else
          if !model.nil?

            if workingName != model.class.name.underscore.pluralize.dasherize
              workingName = model.class.name.underscore.pluralize.dasherize
              @csv << "\n#{model.class.name.underscore.pluralize.dasherize}"
            end
            @csv << ","  << model.id
          end
        end
      end
      @csv << "\n"
    end

    def append_xml(array, filterDate, sync_mode)
      workingName = ""
      array.each do |model|

        if model.instance_of?(Array)
          model.each do |row|
            if !row.nil? && ((row.respond_to?("updated_at") && !row.updated_at.nil? && row.updated_at > filterDate) || sync_mode == :full_sync)
              if workingName != model[0].class.name.underscore.pluralize.dasherize && workingName != ""
                @xml << "</#{workingName}>\n"
                workingName = model[0].class.name.underscore.pluralize.dasherize
                @xml << "<#{workingName}>\n"
              end

              if workingName == ""
                workingName = model[0].class.name.underscore.pluralize.dasherize
                @xml << "<#{workingName}>\n"
              end
              @xml << row.to_xml(:skip_instruct => true)
            end
          end
        else
          if !model.nil? && ((model.respond_to?("updated_at") && !model.updated_at.nil? && model.updated_at > filterDate)  || sync_mode == :full_sync)
            if workingName != model.class.name.underscore.pluralize.dasherize
              @xml << "</#{workingName}>\n"
              workingName = model.class.name.underscore.pluralize.dasherize
              @xml << "<#{workingName}>\n"
            end
            @xml << model.to_xml(:skip_instruct => true)
          end
        end

      end
      if !workingName.nil? && workingName != ""
        @xml << "</#{workingName}>\n"
      end
    end

    # this is where you get a list of arbitrary records and will need to verify that the object is okay
    # to save for the current user.
    # if you have a before_save validation filter on all your objects that does this, then you can leave it blank
    def sanitize_records(recordGroup)

      # you will probably want to verify here, too, that recordGroup is a valid type of object
      # return false, and none of the objects will get saved

      recordGroup.each do |singleRecord|
        if singleRecord.respond_to?("organization_id")
          singleRecord.organization_id = Domain.current.organization_id
        end
      end

      return true
    end

    #
    # todo -- build in something here to check against previous sync log, to see if a given id trying to get deleted ever got sync'd down
    # otherwise, sanitize in whatever way you see fit. This is CSV of id's to get deleted, just like the valididblock in downsync
    #
    def sanitize_deletes(toDelete)
    return true    
  end

end

