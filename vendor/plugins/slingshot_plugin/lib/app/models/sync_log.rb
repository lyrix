class SyncLog < ActiveRecord::Base
  validates_presence_of   :sync_time
  validates_presence_of   :sync_direction
  validates_presence_of   :csv_block   
  validates_presence_of   :status
end