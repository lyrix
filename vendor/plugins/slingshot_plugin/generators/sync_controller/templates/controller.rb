#
# Copyright (c) 2007 Joyent Inc.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


class SlingshotSyncController < SlingshotController
  before_filter :require_http_authentication
  
  # Here is where the bulk of your efffort will go in the down sync portion
  # You are tasked with creating an array of arrays that will define all the data which will be available for a particular
  # user while running in offline mode. This example is simply aggregating all the information for a
  # particular 'organization' but you can embed whatever logic that you need here to send down data with much more
  # fine grained permissions. You could also, perhaps, delete all the database data from a particular client if they are no longer
  # allowed access to your service.
  def aggregate_data
      # example for Radiant
      # model = [Page.find(:all)]
      # model += PagePart.find(:all)
  end
  
  #
  #  Not required, but a good place to have a helper method to download a file using http auth
  #  so that you can easily sync down files.
  #  When sync happens, you're going to have a user requesting info without having an active session
  #
  def download_file
    # Example
    # @joyent_file = JoyentFile.restricted_find(params[:id])
    #     send_joyent_file(@joyent_file, true)
    #   rescue ActiveRecord::RecordNotFound
    #     redirect_to files_index_url
  end
  
  
  def require_http_authentication
    return true
    # Use this to hook into your authentication
    
    # unless (auth = (request.env['X-HTTP_AUTHORIZATION'] || request.env['HTTP_AUTHORIZATION'])).nil?
    #       auth = auth.split
    #       user, password = Base64.decode64(auth[1]).split(':')[0..1]
    #       if user = Domain.current.authenticate_user(user,password)
    #         User.current=user
    #         return true
    #       end
    #     end
    #     @response.headers["Status"] = "Unauthorized"
    #     @response.headers["WWW-Authenticate"] = "Basic realm=\"Offline Sync from Joyent\""
    #     render :text => _('You must log in to access your Sync'), :status => 401
  end

end

