class SyncControllerGenerator < Rails::Generator::Base
  
  def initialize(*runtime_args)
    super(*runtime_args)
  end
  
  def manifest
    record do |m|
      m.class_collisions "SlingshotSyncController"

      m.template 'controller.rb',     File.join('app/controllers', "slingshot_sync_controller.rb")
    end
  end
end