class CreateSongs < ActiveRecord::Migration
  def self.up
    create_table :songs do |t|
      t.column :user_id, :integer
      t.column :title, :string, :null => false
      t.column :artist, :string
      t.column :lyrics, :text
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
  end

  def self.down
    drop_table :songs
  end
end
