class CreateShows < ActiveRecord::Migration
  def self.up
    create_table :shows do |t|
      t.column :user_id, :integer
      t.column :title, :string, :null => false
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
  end

  def self.down
    drop_table :shows
  end
end
