class CreateUsages < ActiveRecord::Migration
  def self.up
    create_table :usages do |t|
      t.column :show_id, :integer
      t.column :song_id, :integer
      t.column :position, :integer
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
  end

  def self.down
    drop_table :usages
  end
end
