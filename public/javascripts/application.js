Lyrix = {}; // Our own little namespace.

// We can pass values from the server this way
// I like this syntax better
$P = function(element) {
  return $(element).innerHTML;
}

// ===============================================
// = Behavior proxies to Script.aculo.us objects =
// ===============================================
Lyrix.Draggable = Behavior.create({
  initialize: function(options) {
    this.options = Object.extend({
      revert : true
    }, options || {});
    this.draggable = new Draggable(this.element, this.options);
  }
});

Lyrix.Droppable = Behavior.create({
  initialize: function(options) {
    this.options = (options || {});
    Droppables.add(this.element, this.options);
  }
});

Lyrix.Sortable = Behavior.create({
  initialize: function(options) {
    this.options = Object.extend({
      constraint: false
    }, options || {});
    Sortable.create(this.element, this.options);
  }
});

Lyrix.InPlaceEditor = Behavior.create({
  initialize: function(options) {
    this.options = Object.extend({
      urlBuilder: this._makeRequestURL,
      ajaxOptions: {method: 'put'}
    }, options || {});
    
    this.url = this.options.urlBuilder(this.element);
    this.options.urlBuilder = null;
    
    new Ajax.InPlaceEditor(this.element, this.url, this.options);
  },
  // Builds a default
  _makeRequestURL: function(element) {
    split_id = element.id.split('_');
    
    params = {object: split_id[0], method: split_id[1], id: split_id[2]};
    return '/' + params.object + 's/set_' + params.object + '_' + params.method + '/' + params.id;
  }
});

// ====================
// = Custom behaviors =
// ====================
Lyrix.DeleteLink = Behavior.create({
  initialize: function() {
    this.form = $form({
      style: 'display:none',
      method: 'post',
      action: this.element.href
    }, $input({
      type: 'hidden',
      name: '_method',
      value: 'delete'
    }));
    
    this.element.appendChild(this.form);
  },
  onclick: function(e) {
    this.form.submit();
    Event.stop(e);
  }
});

Lyrix.FadingMessage = Behavior.create({
  onclick: function(e) {
    new Effect.Fade(this.element);
  }
})

// =================================
// = Behaviors for the application =
// =================================
Event.addBehavior({
  // Add some magical in-place-editors
  'span.in_place_editor_field': Lyrix.InPlaceEditor,
  
  // Delete things using a hidden form
  'a.deletable': Lyrix.DeleteLink,
  
  // Notices should fade out when clicked
  '#notice': Lyrix.FadingMessage,
  
  // Allow songs to be dragged back and forth
  '.song, .usage': Lyrix.Draggable({handle: 'draggable'}),
  
  // Make the songs in a show sortable and
  // allow songs from the sidebar to be dropped in
  '#songs_list': function() {
    sortableOptions = {
      constraint: false,
      handle: 'draggable',
      onUpdate: function(element) {
        new Ajax.Request('/shows/' + $P('show_id') + '/reorder', {
          method: 'put',
          parameters: Sortable.serialize(element)
        });
      }
    };
    
    Lyrix.Droppable.attach(this, {
      accept: 'song',
      onDrop: function(element) {
        new Ajax.Request('/usages?show_id=' + $P('show_id'), {
          parameters: 'id=' + encodeURIComponent(element.id.gsub('song_', '')),
          onComplete: function() {
            // Make sure the sortable works for new elements
            Sortable.create('songs_list', sortableOptions);
          }
        });
      }
    });
    
    Lyrix.Sortable.attach(this, sortableOptions);
  },
  
  // Allow songs from a show to be dropped out of the show
  '#side_songs_list': Lyrix.Droppable({
    accept: 'usage',
    onDrop: function(element) {
      id = encodeURIComponent(element.id.gsub('usage_', ''));
      new Ajax.Request('/usages/' + id, {
        method: 'delete'
      });
    }
  })
});