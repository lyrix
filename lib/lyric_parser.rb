module Lyrix
  class LyricParser
    attr_reader :lyrics
    
    DEFAULT_OPTIONS = {} unless defined?(DEFAULT_OPTIONS)
  
    def initialize(lyrics, options = {})
      @options = DEFAULT_OPTIONS.merge(options)
      @lyrics = lyrics
    end
    
    def lyrics=(lyrics)
      @lyrics = lyrics
      @parsed_lyrics = nil
    end
    
    def parse
      @parsed_lyrics ||= parse!
      @parsed_lyrics
    end
    
    def parse!
      parsed = {}
      
      parsed[:slides] = @lyrics.strip.gsub("\r\n", "\n").split(/\n[ \t]*\n/)
      parsed[:slides].collect! { |s| s.strip }
      parsed[:slides].reject! { |s| s.empty? }
      
      parsed[:slides].each_with_index do |s,idx|
        [:chorus, :bridge].each do |sub|
          if s =~ Regexp.new("^\\[#{sub}\\]") and !parsed.has_key?(sub)
            parsed[sub] = s.split("\n")[1..-1].join("\n")
            parsed[:slides][idx] = parsed[sub]
          end
        
          parsed[:slides][idx].gsub!("[#{sub}]", parsed[sub]) if parsed[sub]
        end
        
        if s =~ /^\[([2-9])x\]/
          parsed[:slides][idx] = (1..($1.to_i)).collect { s.split("\n")[1..-1].join("\n") }
        end
      end
      
      parsed[:slides].flatten!
      
      parsed[:slides].each_with_index do |s,idx|
        slide = parsed[:slides][idx].split("\n")
        slide.each_with_index do |line,i|
          if line.strip =~ /\[([2-9])x\]$/
            num = $1.to_i
            line = line.gsub("[#{num}x]", "").strip
            slide[i] = (1..num).collect { line }
          end
        end
        parsed[:slides][idx] = slide.flatten.join("\n")
      end
      
      parsed
    end
    
    def to_slides
      parse[:slides]
    end
  end
end