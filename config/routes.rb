ActionController::Routing::Routes.draw do |map|
  map.home '', :controller => 'songs', :action => 'index'
  map.resources :songs, :usages, :users, :sessions
  map.resources :shows, :member => {:reorder => :put}
  
  map.activate 'activate/:activation_code', :controller => 'users', :action => 'activate'
  
  # Slingshot routes
  map.with_options(:controller => 'slingshot_sync') do |sync|
    sync.up   'sync/up', :action => 'up'
    sync.down 'sync/down', :action => 'down'
    sync.log  'sync/log', :action => 'log'
  end

  # Install the default route as the lowest priority.
  map.connect ':controller/:action/:id.:format'
  map.connect ':controller/:action/:id'
end
