set :application, "lyrix"
set :repository,  "git://repo.or.cz/lyrix.git"
set :user, "mattmoriarity"

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :deploy_to, "/var/www/apps/#{application}"

# If you aren't using Subversion to manage your source code, specify
# your SCM below:
set :scm, :git
set :git_dir, "#{shared_path}/git"

set :domain, "ubuntu"

role :app, domain
role :web, domain
role :db,  domain, :primary => true

set :runner, "mattmoriarity"

before "deploy:setup", :db
after "deploy:update_code", "db:symlink"

namespace :db do
  desc "Create database yaml in shared path"
  task :default do
    db_config = ERB.new <<-EOF
base: &base
  adapter: mysql
  host: localhost
  username: #{user}
  password: PASSWORD

development:
  database: #{application}_dev
  <<: *base

test:
  database: #{application}_test
  <<: *base

production:
  database: #{application}_prod
  <<: *base
    EOF

    run "mkdir -p #{shared_path}/config"
    put db_config.result, "#{shared_path}/config/database.yml"
  end

  desc "Make symlink for database yaml"
  task :symlink do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end

namespace :deploy do 
  desc <<-DESC 
    Sets up the git repository clones to :git_dir on the app servers. 
  DESC
  task :git_clone, :except => { :no_release => true } do 
    run(source.clone) 
  end 
end