class Show < ActiveRecord::Base
  belongs_to :user, :dependent => :destroy
  has_many :usages, :order => :position, :dependent => :destroy
  has_many :songs, :through => :usages, :order => :position
  
  validates_presence_of :title
  
  def order!(*sorted_ids)
    transaction do
      sorted_ids.flatten.each_with_index do |usage, pos|
        usages.find(usage).update_attributes(:position => pos + 1)
      end
      save
    end
  end
end
