require_dependency 'lyric_parser'

class Song < ActiveRecord::Base
  belongs_to :user, :dependent => :destroy
  has_many :usages, :dependent => :destroy
  has_many :shows, :through => :usages
  
  # The entire UI breaks down without a title
  validates_presence_of :title
  
  # Split the song into slides.
  #
  # Slides are separated by a blank line, indicated
  # by two new-line characters. The parser is pretty
  # tolerant of whitespace. See the RSpecs for examples.
  def slides
    Lyrix::LyricParser.new(lyrics).to_slides
  end
  
  def artist
    attributes["artist"] || ''
  end
end
