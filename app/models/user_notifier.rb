# Delivers e-mails after signup and activation.
class UserNotifier < ActionMailer::Base
  def signup_notification(user)
    setup_email(user)
    @subject    += 'Please activate your new account'
  
    @body[:url]  = "http://mylyrix.ath.cx/activate/#{user.activation_code}"
  
  end
  
  def activation(user)
    setup_email(user)
    @subject    += 'Your account has been activated!'
    @body[:url]  = "http://mylyrix.ath.cx/"
  end
  
  protected
    def setup_email(user)
      @recipients  = "#{user.email}"
      @from        = "donotreply@mylyrix.ath.cx"
      @subject     = "[Lyrix] "
      @sent_on     = Time.now
      @body[:user] = user
    end
end
