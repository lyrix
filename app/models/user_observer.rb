class UserObserver < ActiveRecord::Observer
  # Deliver a notification after the user signs up.
  def after_create(user)
    UserNotifier.deliver_signup_notification(user)
  end

  # Deliver a confirmation after the user activates.
  def after_save(user)
    UserNotifier.deliver_activation(user) if user.recently_activated?
  end
end
