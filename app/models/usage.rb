class Usage < ActiveRecord::Base
  belongs_to :song
  belongs_to :show
  
  acts_as_list :scope => :show
end
