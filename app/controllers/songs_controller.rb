# Handles dealing with songs
# 
# We use make_resourceful to create most of the
# RESTful actions of the controller. We use a custom
# new action because it behaves a little different,
# but otherwise the actions are generated by
# make_resourceful.
class SongsController < ApplicationController
  in_place_edit_for :song, :title
  in_place_edit_for :song, :artist
  
  helper :lyrics
  
  make_resourceful do
    # All but new. It's special
    actions :index, :show, :edit, :update, :destroy
    
    # Redirect to / instead of /songs
    response_for(:destroy) do 
      set_default_flash :notice, "The song was deleted."
      set_default_redirect home_path
    end
    # Easier to do multiple edits
    response_for(:update) do
      set_default_flash :notice, "The song was saved."
      set_default_redirect edit_object_path
    end
    
    # Not quite ready yet
    # publish :html, :xml
  end
  
  # Creates a new song and lets you edit it.
  #
  # Instead of just showing a blank form, we create a new
  # song in the database first. This lets us use the same
  # in-place editors and forms for both views. Also helps
  # maintain sidebar consistency.
  def new
    @song = current_model.create(:title => 'Untitled')
    redirect_to edit_song_path(@song)
  end
  
  protected
    # Make all songs hang off of the current user
    #
    # This serves two purposes:
    # - Makes sure new songs belong to the current user
    # - Makes sure the user can't access songs that belong
    #   to them
    def current_model
      current_user.songs
    end
end
