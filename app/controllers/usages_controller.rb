# Handles the links between songs and controllers.
# 
# These are created in the view by drag-and-drop,
# but that's probably not important to the controller.
class UsagesController < ApplicationController
  # Add a song to a show
  def create
    @usage = Usage.create(:show_id => params[:show_id], :song_id => params[:id])
  end
  
  # Remove a song from a show
  def destroy
    @usage = Usage.destroy(params[:id])
  end
end
