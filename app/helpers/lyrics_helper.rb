module LyricsHelper
  def lyric_slide(slide)
    split_slide = slide.gsub("\r\n", "\n").split("\n")
    first_line = split_slide.first
    slide = split_slide[1..-1].join("\r\n")
    
    html = "<div class='slide lyric_slide'>"
    html << RedCloth.new("h1. #{first_line}").to_html
    html << "<div class='lyrics'>"
    html << RedCloth.new(slide, [:hard_breaks]).to_html
    html << "</div></div>"
    
    html
  end
  
  def slides_for_song(song)
    RedCloth.new(song.slides.join("<hr />"), [:hard_breaks]).to_html
  end
end
