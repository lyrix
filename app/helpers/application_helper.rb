# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def song_class(song)
    (!@song.nil? and @song.id == song.id) ? 'current song' : 'song'
  end
  
  def show_class(show)
    (!@show.nil? and @show.id == show.id) ? 'current show' : 'show'
  end
  
  def all_songs
    logged_in? ? current_user.songs.find(:all, :order => 'title') : []
  end
  
  def all_shows
    logged_in? ? current_user.shows.find(:all, :order => 'created_at DESC') : []
  end
  
  def in_place_options(object, method)
    object = instance_variable_get("@#{object}")
    object_type = object.class.to_s.underscore
    {:class => "in_place_editor_field", :id => "#{object_type}_#{method}_#{object.id}_in_place_editor"}
  end
  
  def notice_options
    default_options = {:id => 'notice'}
    flash.has_key?(:notice) ? default_options : default_options.merge({:style => 'display: none'})
  end
end
