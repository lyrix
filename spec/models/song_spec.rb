require File.dirname(__FILE__) + '/../spec_helper'

describe Song do
  before(:each) do
    @song = Song.new
  end
  
  it "should require a title" do
    @song.title = ''
    @song.artist = 'David Crowder Band'
    @song.lyrics = 'La la la la la la'
    @song.should_not be_valid
    @song.should have(1).error_on(:title)
    
    @song.title = 'Every Move I Make'
    @song.should be_valid
    @song.should have(:no).errors_on(:title)
  end

end

describe Song, "with an artist" do
  before(:each) do
    @song = Song.new
  end
  
  it "should return the artist" do
    @song.artist = "An Artist"
    @song.artist.should_not be_nil
  end
end

describe Song, "without an artist" do
  before(:each) do
    @song = Song.new
  end
  
  it "should return an empty string for the artist" do
    @song.artist.should_not be_nil
    @song.artist.should == ''
  end
end