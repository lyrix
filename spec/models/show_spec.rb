require File.dirname(__FILE__) + '/../spec_helper'

describe Show do
  before(:each) do
    @usages = mock("proxy")
    
    @show = Show.new
    @show.stub!(:usages).and_return(@usages)
    def @show.transaction
      yield
    end
    
    @usage = mock_model(Usage)
  end

  it "should require a title" do
    @show.title = ''
    @show.should_not be_valid
  end
  
  it "should reorder the songs inside it" do
    [[3,1],[2,2],[1,3]].each do |id,pos|
      @usages.should_receive(:find).with(id).and_return(@usage)
      @usage.should_receive(:update_attributes).with({:position => pos})
    end
    @show.order!([3,2,1])
  end
end
