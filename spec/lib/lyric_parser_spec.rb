require File.dirname(__FILE__) + '/../spec_helper'
require 'lyric_parser'

describe Lyrix::LyricParser do
  before do
    @parser = Lyrix::LyricParser.new("")
  end
  
  it "should split slides by blank lines" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics
Hopefully this will get split up appropriately

This text should be on slide 2
As should this

Here comes slide three
    LYRICS

    @parser.to_slides.size.should == 3
  end

  it "should allow spaces in blank lines of lyrics" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics
Hopefully this will get split up appropriately

This text should be on slide 2
As should this

Here comes slide three
    LYRICS

    @parser.to_slides.size.should == 3
  end

  it "should not include blank slides" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics
Hopefully this will get split up appropriately



This text should be on slide 2
As should this


Here comes slide three


    LYRICS

    @parser.to_slides.size.should == 3
    @parser.to_slides.each { |s| s.should_not == "" }
  end

  it "should substitute a chorus in if given" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics
Hopefully this will get split up appropriately

[chorus]
This text should be on slide 2
As should this

Here comes slide three

[chorus]
    LYRICS

    @parser.to_slides.size.should == 4
    @parser.to_slides[3].should == "This text should be on slide 2\nAs should this"
  end

  it "should substitute a bridge in if given" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics
Hopefully this will get split up appropriately

[bridge]
This text should be on slide 2
As should this

Here comes slide three

[bridge]
    LYRICS

    @parser.to_slides.size.should == 4
    @parser.to_slides[3].should == "This text should be on slide 2\nAs should this"
  end

  it "should substitute a chorus and bridge in if both are given" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics
Hopefully this will get split up appropriately

[chorus]
This text should be on slide 2
As should this

[bridge]
Here comes slide three

[chorus]

Some content

[bridge]
    LYRICS

    @parser.to_slides.size.should == 6
    @parser.to_slides[3].should == "This text should be on slide 2\nAs should this"
    @parser.to_slides[1].should == @parser.to_slides[3]
    @parser.to_slides[2].should == @parser.to_slides[5]
  end
  
  it "should repeat whole slides if asked" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics
Hopefully this will get split up appropriately

[2x]
This text should be on slide 2
As should this

[3x]
Here comes slide three
    LYRICS

    @parser.to_slides.size.should == 6
  end
  
  it "should repeat single lines if asked" do
    @parser.lyrics = <<-LYRICS
This is a test of the lyrics [2x]
Hopefully this will get split up appropriately

This text should be on slide 2
As should this

Here comes slide three
    LYRICS

    @parser.to_slides.size.should == 3
    @parser.to_slides[0].split("\n").size.should == 3
  end
end