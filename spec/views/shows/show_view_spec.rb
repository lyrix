require File.dirname(__FILE__) + '/../../spec_helper'

describe "/shows/show" do
  before do
    song = mock_model(Song, :title => 'test song', :artist => 'test artist', :slides => ['slide1', 'slide2'])
    @show = mock_model(Show, :title => 'test show', :songs => [song])
    
    @controller.template.stub!(:lyric_slide).and_return(%Q[
    <div class="lyric_slide slide">
      <h1>first</h1>
      <div class="lyrics">
        <p>test<br/>
        test<br/>
        test</p>
      </div>
    </div>
    ])
  end
  
  it "should have a title slide with the show name" do
    assigns[:show] = @show
    render '/shows/show'
    response.should have_tag('div.slide.title_slide') do
      with_tag('h1', 'test show')
    end
  end
  
  it "should have a title slide for each song" do
    assigns[:show] = @show
    render '/shows/show'
    response.should have_tag('div.presentation') do
      with_tag('div.slide.title_slide') do
        with_tag('h1', 'test song')
        with_tag('h2', 'test artist')
      end
    end
  end
  
  it "should have slides for lyrics" do
    assigns[:show] = @show
    render '/shows/show'
    response.should have_tag('div.slide.lyric_slide') do
      with_tag('h1', 'first')
      with_tag('div.lyrics') do
        with_tag('p')
      end
    end
  end
end