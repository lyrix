require File.dirname(__FILE__) + '/../../spec_helper'

describe "/shows/edit" do
  before do
    @song = mock_model(Song, :title => 'test song')
    @usage = mock_model(Usage, :song => @song)
    @show = mock_model(Show, :title => 'test show', :usages => [@usage])
  end
  
  it "should contain an in-place editor for title" do
    assigns[:show] = @show
    render '/shows/edit'
    response.should have_tag("span.in_place_editor_field#show_title_#{@show.id}_in_place_editor")
  end
  
  it "should contain an ordered list of songs in the show" do
    assigns[:show] = @show
    render '/shows/edit'
    response.should have_tag('ol#songs_list') do
      with_tag("li.usage#usage_#{@usage.id}")
    end
  end
end