require File.dirname(__FILE__) + '/../../spec_helper'

describe "/songs/edit" do
  before do
    @song = mock_model(Song, :title => 'test song', :artist => '', :lyrics => 'test lyrics')
  end
  
  it "should contain an in-place editor for song title" do
    assigns[:song] = @song
    render '/songs/edit'
    response.should have_tag("span.in_place_editor_field#song_title_#{@song.id}_in_place_editor")
  end
  
  it "should contain an in-place editor for song artist" do
    assigns[:song] = @song
    render '/songs/edit'
    response.should have_tag("span.in_place_editor_field#song_artist_#{@song.id}_in_place_editor")
  end
  
  it "should contain a textarea for song lyrics" do
    assigns[:song] = @song
    render '/songs/edit'
    response.should have_tag('textarea#song_lyrics', 'test lyrics')
  end
end