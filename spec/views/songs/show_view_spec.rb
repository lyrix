require File.dirname(__FILE__) + '/../../spec_helper'

describe "/songs/show" do
  before do
    @song = mock_model(Song, :title => 'test song', :artist => '')
    
    @slides = 'first<br/>test<br/>test<hr />second<br/>test<br/>test<hr />third<br/>test<br/>test'
    
    @controller.template.stub!(:slides_for_song).and_return(@slides)
  end
  
  it "should show a heading with the song title" do
    assigns[:song] = @song
    render '/songs/show'
    response.should have_tag('h1', 'test song')
  end
  
  it "should show a heading with the song artist" do
    @song.stub!(:artist).and_return('test artist')
    assigns[:song] = @song
    render '/songs/show'
    response.should have_tag('h2', 'test artist')
  end
  
  it "should not contain a heading for artist if artist is blank" do
    assigns[:song] = @song
    render '/songs/show'
    response.should_not have_tag('h2')
  end
end