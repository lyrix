require File.dirname(__FILE__) + '/../spec_helper'

describe ShowsController, "logged in" do
  before do
    @show = mock_model(Show)
    @shows_proxy = mock("proxy")
    @controller.stub!(:logged_in?).and_return(true)
    @controller.stub!(:current_model).and_return(@shows_proxy)
  end
  
  it "should create a new show and redirect to edit on new" do
    @shows_proxy.should_receive(:create).with(:title => 'Untitled').and_return(@show)
    get 'new'
    @response.should redirect_to("/shows/#{@show.id}/edit")
  end
  
  it "should update positions of all songs in show on reorder" do
    @shows_proxy.should_receive(:find).and_return(@show)
    @show.should_receive(:order!).with(['2', '1'])
    put 'reorder', :id => '1', :songs_list => ['2', '1']
  end
  
  it "should redirect to home on index" do
    get 'index'
    response.should redirect_to('/')
  end
  
  it "should redirect to home on destroy" do
    @shows_proxy.should_receive(:find).and_return(@show)
    @show.should_receive(:destroy).and_return(true)
    delete 'destroy', :id => '1'
    response.should redirect_to('/')
  end
  
  it "should display a message to the user on destroy" do
    @shows_proxy.should_receive(:find).and_return(@show)
    @show.should_receive(:destroy).and_return(true)
    delete 'destroy', :id => '1'
    flash[:notice].should_not be_blank
  end
end
