require File.dirname(__FILE__) + '/../spec_helper'

describe UsagesController do
  before do
    @usage = mock_model(Usage)
    @controller.stub!(:logged_in?).and_return(true)
  end
  
  it "should create a new usage on create" do
    Usage.should_receive(:create).and_return(@usage)
    post 'create', :show_id => '1', :id => '2'
    assigns[:usage].should == @usage
  end
  
  it "should delete an old usage on delete" do
    Usage.should_receive(:destroy).and_return(@usage)
    delete 'destroy', :id => '1'
    assigns[:usage].should == @usage
  end
end