require File.dirname(__FILE__) + '/../spec_helper'

describe SongsController, "logged in" do
  before do
    @song = mock_model(Song)
    @songs_proxy = mock("proxy")
    @controller.stub!(:logged_in?).and_return(true)
    @controller.stub!(:current_model).and_return(@songs_proxy)
  end
  
  it "should create a new song and redirect to edit on new" do
    @songs_proxy.should_receive(:create).with(:title => 'Untitled').and_return(@song)
    get 'new'
    @response.should redirect_to("/songs/#{@song.id}/edit")
  end
  
  it "should redirect to home on destroy" do
    @songs_proxy.should_receive(:find).and_return(@song)
    @song.should_receive(:destroy).and_return(true)
    delete 'destroy', :id => '1'
    response.should redirect_to('/')
  end
  
  it "should display a message to the user on destroy" do
    @songs_proxy.should_receive(:find).and_return(@song)
    @song.should_receive(:destroy).and_return(true)
    delete 'destroy', :id => '1'
    flash[:notice].should_not be_blank
  end
  
  it "should redirect to edit page after update" do
    @songs_proxy.should_receive(:find).and_return(@song)
    @song.should_receive(:update_attributes).and_return(true)
    put 'update', :id => '1', :song => {:lyrics => "blah"}
    response.should redirect_to("/songs/#{@song.id}/edit")
  end
  
  it "should display a message to the user on update" do
    @songs_proxy.should_receive(:find).and_return(@song)
    @song.should_receive(:update_attributes).and_return(true)
    put 'update', :id => '1', :song => {:lyrics => "blah"}
    flash[:notice].should_not be_blank
  end
end
