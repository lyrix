require File.dirname(__FILE__) + '/../spec_helper'

describe LyricsHelper do
  before do
    @song = Song.new
    @song.lyrics = <<-LYRICS
This is a *test* of the lyrics
Hopefully this will get split up appropriately

This text should be on slide 2
As should this

Here comes slide three
    LYRICS
  end
  
  it "should add hr tags in between slides" do
    text = slides_for_song(@song)
    text.split("<hr />").size.should == 3
  end
  
  it "should use Textile formatting in lyrics" do
    text = slides_for_song(@song)
    text.should include("<strong>")
  end
  
  it "should create a properly-formatted lyrics slide" do
    result = lyric_slide("first\ntest\ntest\ntest")
    
    result.split("<br />").size.should == 3
    result.should include("<h1>first</h1>")
    result.should include("<p>test<br />test<br />test</p>")
  end
end
