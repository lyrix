require File.dirname(__FILE__) + '/../spec_helper'

describe ApplicationHelper do
  it "should generate an id and class for an in-place editor" do
    @song = mock_model(Song)
    result = in_place_options(:song, :title)
    
    result[:id].should include("song_title_#{@song.id}")
    result[:class].should == 'in_place_editor_field'
  end
end

describe ApplicationHelper, "with a current song" do
  it "should give the current song a current CSS class" do
    @song = mock_model(Song)
    song_class(@song).should include('current')
  end
  
  it "should not give a non-current song a current CSS class" do
    @song = mock_model(Song)
    song_class(mock_model(Song)).should_not include('current')
  end
end

describe ApplicationHelper, "without a current song" do
  it "should not give a current CSS class if there is no current song" do
    song_class(mock_model(Song)).should_not include('current')
  end
end

describe ApplicationHelper, "with a current show" do
  it "should give the current show a current CSS class" do
    @show = mock_model(Show)
    show_class(@show).should include('current')
  end
  
  it "should not give a non-current show a current CSS class" do
    @show = mock_model(Show)
    show_class(mock_model(Show)).should_not include('current')
  end
end

describe ApplicationHelper, "without a current show" do
  it "should not give a current CSS class if there is no current show" do
    show_class(mock_model(Show)).should_not include('current')
  end
end

describe ApplicationHelper, "logged in" do
  before do
    @songs = mock("songs_proxy")
    @shows = mock("shows_proxy")
    @user = mock_model(User, :songs => @songs, :shows => @shows)
    stub!(:logged_in?).and_return(true)
    stub!(:current_user).and_return(@user)
  end
  
  it "should get a list of all the user's songs" do
    @songs.should_receive(:find).and_return(["a song"]) # who cares if it returns a string
    all_songs.size.should == 1
  end
  
  it "should get a list of all the user's shows" do
    @shows.should_receive(:find).and_return(["a show"]) # again, who cares?
    all_shows.size.should == 1
  end
end

describe ApplicationHelper, "logged out" do
  before do
    stub!(:logged_in?).and_return(false)
  end
  
  it "should retrieve an empty list of songs" do
    all_songs.size.should == 0
  end
  
  it "should retrieve an empty list of shows" do
    all_shows.size.should == 0
  end
end

describe ApplicationHelper, "with a flash notice" do
  before do
    @flash = mock("flash")
  end
  
  it "should display notice box if there is a flash notice" do
    @flash.should_receive(:has_key?).with(:notice).and_return(true)
    notice_options.should_not have_key(:style)
  end
end

describe ApplicationHelper, "without a flash notice" do
  before do
    @flash = mock("flash")
  end
  
  it "should not display notice box if there is no flash notice" do
    @flash.should_receive(:has_key?).with(:notice).and_return(false)
    notice_options.should have_key(:style)
  end
end